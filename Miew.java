import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Miew
{

    public static void main(String[] args)
    {
        customerlist c = new customerlist();
        requestlist r = new requestlist();
        rabetkarbari x = new rabetkarbari();
        addkalarequest e = new addkalarequest();
        sellerlist lm = new sellerlist();
        kmobile km= new kmobile();
        klaptop kl = new klaptop();
        klebas kle = new klebas();
        kshoes ksh = new kshoes();
        kref kr = new kref();
        kgas kg = new kgas();
        ktv kt = new ktv();
        keating ke = new keating();
        listkala l = new listkala();
        try
        {
            x.mypanel();
            rabetkarbari.safheasli();
        }
        catch (emailinvalid ei)
        {
            main.print(ei.getMessage());
        }
        catch (invalidphonenum iph)
        {
            main.print(iph.getMessage());
            rabetkarbari.safheasli();

        }

    }
    static void print(Object obj)
    {
        System.out.println(obj);
    }
    static double scanDouble()
    {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        return x;
    }
    static int scanInt()
    {
        Scanner scanner = new Scanner(System.in);
        int y = scanner.nextInt();
        return y;
    }
    static String scanString()
    {
        Scanner scanner = new Scanner(System.in);
        String z = scanner.nextLine();
        return z;
    }
    static Long scanLong()
    {
        Scanner scanner = new Scanner(System.in);
        Long l = scanner.nextLong();
        return l;
    }
}
//______________________________________________________________________________________________________________________

class invalidbuy extends Exception
{
    invalidbuy()
    {
        super("invalid buy");
    }
    invalidbuy(String msg)
    {
        super(msg);
    }
}
class invalidinput extends Exception
{
    invalidinput()
    {
        super("invalid input");
    }
    invalidinput(String msg)
    {
        super(msg);
    }
}
class emailinvalid extends invalidinput
{
    emailinvalid()
    {
        super("please inter the right form of email");
    }
}
class invalidphonenum extends invalidinput
{
    invalidphonenum()
    {
        super("please inter the right form of phone num");
    }
}
class invalidcharge extends invalidbuy
{
    invalidcharge()
    {
        super("sorry but you do not have enough money");
    }
}
class invalidmojudi extends invalidbuy
{
    invalidmojudi()
    {
        super("sorry but this product is not available anymore");
    }
}
class account

{   private int charge;
    private int id;
    private String name;
    private String lastname;
    private String username;
    private String email;
    private String password;
    private long phonenumber;

    account(String name, String lastname,String username,String email,String password,long phonenumber , int charge)
    {
        setCharge(charge);
        setName(name);
        setEmail(email);
        setLastname(lastname);
        setPassword(password);
        setPhonenumber(phonenumber);
        setUsername(username);
    }
    void setName(String name)
    {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    void setLastname(String lastname)
    {
        this.lastname = lastname;
    }
    void setUsername(String username)
    {
        this.username = username;
    }
    void setEmail(String email)
    {
        this.email = email;
    }
    void setPassword(String password)
    {
        this.password = password;
    }
    void setPhonenumber(long phonenumber)
    {
        this.phonenumber = phonenumber;
    }
    public String getPassword()
    {
        return password;
    }
    public String getUsername()
    {
        return username;
    }
    public long getPhonenumber()
    {
        return phonenumber;
    }
    public String getEmail()
    {
        return email;
    }
    public String getLastname()
    {
        return lastname;
    }
    public String getName()
    {
        return name;
    }
    public int getCharge() {
        return charge;
    }
    public void setCharge(int charge) {
        this.charge = charge;
    }
}
class seller extends account
{
    private String factoryname;
    seller(String name, String lastname,String username,String email,String password,long phonenumber,String factoryname,int charge)
    {
        super( name ,lastname ,username ,email ,password ,phonenumber,charge);
        setFactoryname(factoryname);
    }
    void setFactoryname(String factoryname)
    {
        this.factoryname = factoryname;
    }
    public String getFactoryname()
    {
        return factoryname;
    }
    void changeSellername(String str)
    {
        setName(str);
    }
    void changeSellerlastname(String str)
    {
        setLastname(str);
    }
    void changeSelleremail(String str)
    {
        setEmail(str);
    }
    void changeSellerphonenumber(long l)
    {
        setPhonenumber(l);
    }
    void changeSellerpassword(String str)
    {
        setPassword(str);
    }
    void changeSellerfactoryname(String str)
    {
        setFactoryname(str);
    }

}
class customer extends account
{
    private String offcode;
    customer(String name, String lastname,String username,String email,String password,long phonenumber, int charge)
    {
        super( name,  lastname, username, email, password, phonenumber, charge);
    }
    void changeCustomername(String str)
    {
        setName(str);
    }
    void changeCustomerlastname(String str)
    {
        setLastname(str);
    }
    void changeCustomeremail(String str)
    {
        setEmail(str);
    }
    void changeCustomerphonenumber(long l)
    {
        setPhonenumber(l);
    }
    void changeCustomerpassword(String str)
    {
        setPassword(str);
    }

    public String getOffcode() {
        return offcode;
    }
    public void setOffcode(String offcode) {
        this.offcode = offcode;
    }
}
class manager extends account
{
    manager()
    {
        super("amina" , "shojaie" , "manager" , "www.aminashojaies@gmail.com" , "manager" , 921761483,99);
    }
    void changeManagername(String str)
    {
        setName(str);
    }
    void changeManagerlastname(String str)
    {
        setLastname(str);
    }
    void changeManageremail(String str)
    {
        setEmail(str);
    }
    void changeManagerphonenumber(long l)
    {
        setPhonenumber(l);
    }
    void changeManagerpassword(String str)
    {
        setPassword(str);
    }

}
class customerfactor
{
    private Long shoppingid;
    private String shoppingdate;
    private long shoppingprice;
    private String shoppingsellername;
    private enum shoppingdeliverysituation
    {
        deliverd,in_process
    }
    shoppingdeliverysituation shds;
    ArrayList <kala> shoppinglist;
    customerfactor(Long shoppingid ,String shoppingdate , long shoppingprice ,String shoppingsellername ,shoppingdeliverysituation shds ,ArrayList shoppinglist )
    {
        setShoppingid(shoppingid);
        setShds(shds);
        setShoppingdate(shoppingdate);
        setShoppingprice(shoppingprice);
        setShoppingsellername(shoppingsellername);
        this.shoppinglist = new ArrayList<kala>();
    }
    void setShoppingid(Long shoppingid) {this.shoppingid =shoppingid;}
    void setShoppingdate(String shoppingdate) {this.shoppingdate = shoppingdate;}
    void setShoppingprice(long shoppingprice) {this.shoppingprice = shoppingprice;}
    void setShoppingsellername(String shoppingsellername) {this.shoppingsellername = shoppingsellername;}
    void setShds (shoppingdeliverysituation shds) {this.shds = shds;}

    public ArrayList<kala> getShoppinglist() { return shoppinglist; }
    public Long getShoppingid() { return shoppingid; }
    public long getShoppingprice() { return shoppingprice; }
    public shoppingdeliverysituation getShds() { return shds; }
    public String getShoppingdate() { return shoppingdate; }
    public String getShoppingsellername() { return shoppingsellername; }
}
class sellerfactor
{
    private Long sellingid;
    private String sellingdate;
    private long sellingprice;
    private String sellingcustomername;
    private enum sellingdeliverysituation
    {
        deliverd,in_process
    }
    sellingdeliverysituation sds;
    ArrayList <kala> sellinglist;
    sellerfactor(Long sellingid ,String sellingdate ,long sellingprice ,String sellingcustomername , sellingdeliverysituation sds ,ArrayList sellinglist)
    {
        setSellingid(sellingid);
        setSds(sds);
        setSellingdate(sellingdate);
        setSellingprice(sellingprice);
        setSellingcustomername(sellingcustomername);
        this.sellinglist = new ArrayList<kala>();
    }
    void setSellingid(Long sellingid) {this.sellingid =sellingid;}
    void setSellingdate(String sellingdate) {this.sellingdate = sellingdate;}
    void setSellingprice(long sellingprice) {this.sellingprice = sellingprice;}
    void setSellingcustomername(String sellingcustomername) {this.sellingcustomername = sellingcustomername;}
    void setSds (sellingdeliverysituation  sds) {this.sds = sds;}
}

//__________________________________________________________________________________________________________________

class sellerlist
{
    public static ArrayList <seller> sellers;
    sellerlist()
    {
        this.sellers = new ArrayList<>();
    }
    public static ArrayList<seller> getSellers()
    {
        return sellers;
    }
    public static void setSellers( ArrayList < seller > sellers)
    {
        sellerlist.sellers = sellers;
    }
    static void signupseller(seller seller)
    {
        sellers.add(seller) ;
    }
}
class customerlist
{
    public static ArrayList <customer> customers;
    customerlist()
    {
        this.customers = new ArrayList<>();
    }
    public static ArrayList<customer> getCustomers()
    {
        return customers;
    }
    public static void setCustomers( ArrayList <customer> customers)
    {
        customerlist.customers = customers;
    }
    static void signupcustomer(customer customer)
    {
        customers.add(customer) ;
    }
}
class requestlist
{
    public static ArrayList <seller> waitingsellers;
    requestlist()
    {
        this.waitingsellers = new ArrayList<>();
    }
    public static ArrayList<seller> getWaitingsellers()
    {
        return waitingsellers;
    }
    public static void setWaitingsellers( ArrayList < seller > waitingsellers)
    {
        requestlist.waitingsellers = waitingsellers;
    }
    static void gotorequest(seller seller)
    {
        waitingsellers.add(seller) ;
    }
}
class addkalarequest
{
    public static ArrayList <kala> addrequest;
    addkalarequest() {this.addrequest = new ArrayList<>();}
    public static ArrayList<kala> getAddrequest()
    {
        return addrequest;
    }
    public static void setAddrequest(ArrayList<kala> addrequest)
    {
        addkalarequest.addrequest = addrequest;
    }
}
class listkala
{
    public static ArrayList <kala> sabadekala;
    public static ArrayList <kala> kalalist;
    listkala()
    {
        this.kalalist = new ArrayList<>();
        this.sabadekala = new ArrayList<>();
    }
    public static ArrayList<kala> getKalalist()
    {
        return kalalist;
    }
    public static void setKalalist(ArrayList<kala> kalalist)
    {
        listkala.kalalist = kalalist;
    }
    static void gotokala (kala kala) {kalalist.add(kala);}
}
class kmobile
{
    public static ArrayList<mobile> mobilelist;
    kmobile(){this.mobilelist = new ArrayList<>();}

    public static ArrayList<mobile> getMobilelist() {
        return mobilelist;
    }
    public static void setMobilelist(ArrayList<mobile> mobilelist) {
        kmobile.mobilelist = mobilelist;
    }
    static void gotomobile(kala kala)
    {
        mobilelist.add((mobile) kala);
    }

}
class klaptop
{
    public static ArrayList<laptop> laptoplist;
    klaptop(){this.laptoplist = new ArrayList<>();}
    public static ArrayList<laptop> getLaptoplist() {
        return laptoplist;
    }
    public static void setLaptoplist(ArrayList<laptop> laptoplist) {
        klaptop.laptoplist = laptoplist;
    }
}
class kshoes
{
    public  static ArrayList <shoes> shoeslist ;
    kshoes(){this.shoeslist = new ArrayList<>();}
    public static ArrayList<shoes> getShoeslist() {
        return shoeslist;
    }
    public static void setShoeslist(ArrayList<shoes> shoeslist) {
        kshoes.shoeslist = shoeslist;
    }
}
class klebas
{
    public static ArrayList <lebas> lebaslist;
    klebas(){this.lebaslist = new ArrayList<>();}

    public static ArrayList<lebas> getLebaslist() {
        return lebaslist;
    }
    public static void setLebaslist(ArrayList<lebas> lebaslist) {
        klebas.lebaslist = lebaslist;
    }
}
class kgas
{
    public static ArrayList <gas> gaslist;
    kgas(){this.gaslist = new ArrayList<>();}

    public ArrayList<gas> getGaslist() {
        return gaslist;
    }
    public void setGaslist(ArrayList<gas> gaslist) {
        this.gaslist = gaslist;
    }
}
class kref
{
    public static ArrayList <refregrator> reflist;
    kref()
    {
        this.reflist = new ArrayList<>();
    }
    public ArrayList<refregrator> getReflist() {
        return reflist;
    }
    public void setReflist(ArrayList<refregrator> reflist) {
        this.reflist = reflist;
    }
}
class keating
{
    public static ArrayList <eating> eatlist ;
    keating(){this.eatlist = new ArrayList<>();}

    public static ArrayList<eating> getEatlist() {
        return eatlist;
    }
    public static void setEatlist(ArrayList<eating> eatlist) {
        keating.eatlist = eatlist;
    }
}
class ktv
{
    public static ArrayList<tv> tvlist;
    ktv(){this.tvlist = new ArrayList<>();}

    public static ArrayList<tv> getTvlist() {
        return tvlist;
    }
    public static void setTvlist(ArrayList<tv> tvlist) {
        ktv.tvlist = tvlist;
    }
}

//__________________________________________________________________________________________________________________

class category
{
    private String name;
    private  String special_features;
}
class kala implements Comparable
{
    private long kalaid;
    private String name;
    private String brand;
    private Long price;
    private String seller;
    private int mojudi;
    private String tozihat;
    private int ave_score;
    private ArrayList <String> comments;

    kala( long kalaid ,String name ,String brand ,long price ,String seller ,String tozihat , int mojudi)
    {
        setAve_score(ave_score);
        setKalaid(kalaid);
        setBrand(brand);
        setMojudi(mojudi);
        setName(name);
        setSeller(seller);
        setPrice(price);
        setTozihat(tozihat);
        this.comments = new ArrayList <String>();
    }
    void setName(String name){this.name = name;}
    void setKalaid(long kalaid){this.kalaid = kalaid;}
    void setBrand(String brand){this.brand = brand;}
    void setPrice(long price){ this.price = price;}
    void setSeller(String seller){ this.seller = seller;}
    void setMojudi(int mojudi){ this.mojudi = mojudi;}
    void setTozihat(String tozihat){ this.tozihat = tozihat;}
    void setAve_score(int ave_score){this.ave_score = ave_score;}
    public void setComments(ArrayList<String> comments) {
        this.comments = comments;
    }
    public void setPrice(Long price) {this.price = price;}
    public int getMojudi() {
        return mojudi;
    }
    public long getKalaid() { return kalaid; }
    public int getAve_score() { return ave_score; }
    public String getTozihat() { return tozihat; }
    public Long getPrice() { return price; }
    public String getBrand() { return brand; }
    public String getSeller() { return seller;}
    public String getName() { return name; }
    public ArrayList<String> getComments() { return comments; }

    @Override
    public String toString() {
        return "kala{" +
                "kalaid=" + kalaid +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", seller='" + seller + '\'' +
                ", mojudi=" + mojudi +
                ", tozihat='" + tozihat + '\'' +
                ", ave_score=" + ave_score +
                ", comments=" + comments +
                '}';
    }
    public int compareTo(Object obj)
    {
        kala k1 = (kala) obj;
        if (k1.getName().compareTo(this.getName()) > 0)
            return 1;
        else if (k1.getName().compareTo(this.getName()) < 0)
            return -1;
        else
        {
            if (k1.getAve_score() > this.getAve_score())
                return 1;
            if(k1.getAve_score() < this.getAve_score())
                return -1;
            else
            {
                if (k1.getPrice() > this.getPrice())
                    return 1;
                if (k1.getPrice() < this.getPrice())
                    return -1;
                else
                {
                    if (k1.getMojudi() > this.getMojudi())
                        return 1;
                    if (k1.getMojudi() < this.getMojudi())
                        return -1;
                }
            }
        }
        return 0;
    }
}
class digital extends kala
{
    private int memory;
    private int ram;
    private String systemamel;
    private int weigh;
    private String size;
    digital(int memory , int ram ,String systemamel ,int weigh ,String size ,long kalaid ,String name ,String brand ,long price ,String seller ,String tozihat,int mojudi  )
    {
        super(kalaid ,name ,brand ,price ,seller  , tozihat , mojudi);
        setMemory(memory);
        setRam(ram);
        setSystemamel(systemamel);
        setWeigh(weigh);
        setSize(size);
    }
    void setMemory(int memory) {this.memory = memory;}
    void setRam(int ram) {this.ram = ram;}
    void setSystemamel(String systemamel) {this.systemamel = systemamel;}
    void setWeigh(int weigh) {this.weigh = weigh;}
    void setSize(String size) {this.size = size;}
    public int getMemory() { return memory; }
    public int getRam() { return ram; }
    public int getWeigh() { return weigh; }
    public String getSystemamel() { return systemamel; }
    public String getSize() { return size; }

    @Override
    public String toString() {
        return super.toString() +
                "digital{" +
                "memory=" + memory +
                ", ram=" + ram +
                ", systemamel='" + systemamel + '\'' +
                ", weigh=" + weigh +
                ", size='" + size + '\'' +
                '}';
    }
}
class mobile extends digital
{
    private int cameraquality;
    private int  simcardnum ;

    mobile(int cameraquality , int simcardnum ,int memory , int ram ,String systemamel ,int weigh ,String size ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat ,int mojudi)
    {
        super( memory ,  ram , systemamel ,weigh ,size , kalaid , name , brand , price , seller , tozihat , mojudi);
        setCameraquality(cameraquality);
        setSimcardnum(simcardnum);
    }
    void setCameraquality(int cameraquality){ this.cameraquality = cameraquality; }
    void setSimcardnum(int simcardnum){ this.simcardnum = simcardnum; }

    public int getCameraquality() {
        return cameraquality;
    }
    public int getSimcardnum() {
        return simcardnum;
    }

    @Override
    public String toString() {
        return super.toString() +
                "mobile{" +
                "cameraquality=" + cameraquality +
                ", simcardnum=" + simcardnum +
                '}';
    }
}
class laptop extends digital
{
    private  String pardazande;
    private boolean gaming;
    laptop( String pardazande, boolean gaming ,int memory , int ram ,String systemamel ,int weigh ,String size ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat,int mojudi )
    {
        super( memory ,  ram , systemamel ,weigh ,size , kalaid , name , brand , price , seller , tozihat, mojudi);
        setGaming(gaming);
        setPardazande(pardazande);
    }
    void setPardazande (String pardazande) { this.pardazande = pardazande; }
    void setGaming (boolean gaming) { this.gaming = gaming; }
    public String getPardazande()
    {
        return pardazande;
    }
    public boolean isGaming()
    {
        return gaming;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "laptop{" +
                "pardazande='" + pardazande + '\'' +
                ", gaming=" + gaming +
                '}';
    }
}
class pooshak extends kala
{
    private String materyal;
    private String madein;
    pooshak(String materyal , String madein ,long kalaid ,String name ,String brand ,long price ,String seller ,String tozihat,int mojudi )
    {
        super(kalaid ,name ,brand ,price ,seller  , tozihat, mojudi);
        setMadein(madein);
        setMateryal(materyal);
    }
    void setMateryal (String materyal) { this.materyal = materyal; }
    void setMadein (String madein) { this.madein = madein; }
    public String getMadein() {
        return madein;
    }
    public String getMateryal() {
        return materyal;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "pooshak{" +
                "materyal='" + materyal + '\'' +
                ", madein='" + madein + '\'' +
                '}';
    }
}
class lebas extends pooshak
{
    private String lsize;
    enum tanavo { manto, shalvar ,tshirt}
    private tanavo various;
    lebas(String lsize ,tanavo various ,String materyal , String madein ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat ,int mojudi )
    {
        super(materyal ,madein ,kalaid ,name ,brand ,price ,seller  ,tozihat, mojudi );
        setLsize(lsize);
        setVarious(various);
    }
    void setVarious(tanavo various) { this.various = various; }
    void setLsize(String lsize) { this.lsize = lsize; }
    public String getLsize() {
        return lsize;
    }
    public tanavo getVarious() {
        return various;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "lebas{" +
                "lsize='" + lsize + '\'' +
                ", various=" + various +
                '}';
    }
}
class shoes extends pooshak
{
    private int sizek;
    enum shoestype{ sport , majlesi ,boot}
    private shoes.shoestype sht;
    shoes( int sizek ,shoestype sht ,  String materyal , String madein ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat  ,int mojudi)
    {
        super(materyal ,madein ,kalaid ,name ,brand ,price ,seller  ,tozihat, mojudi);
        setSizek(sizek);
        setSht(sht);
    }
    void setSizek (int sizek) { this.sizek = sizek; }
    void setSht (shoestype sht) {this.sht = sht;}
    public int getSizek() {
        return sizek;
    }
    public shoestype getSht() {
        return sht;
    }

    @Override
    public String toString() {
        return super.toString() +
                "shoes{" +
                "sizek=" + sizek +
                ", sht=" + sht +
                '}';
    }
}
class furniture extends  kala
{
    private String energyuse;
    private boolean garanti;
    furniture(String energyuse ,boolean garanti ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat,int mojudi )
    {
        super(kalaid ,name ,brand ,price ,seller  , tozihat, mojudi);
        setEnergyuse(energyuse);
        setGaranti(garanti);
    }
    void setEnergyuse (String energyuse)  {this.energyuse = energyuse;}
    void setGaranti (boolean garanti) {this.garanti = garanti;}
    public String getEnergyuse() {
        return energyuse;
    }

    @Override
    public String toString()
    {
        return   super.toString() +
                "furniture{" +
                "energyuse='" + energyuse + '\'' +
                ", garanti=" + garanti +
                '}';
    }
}
class tv extends furniture
{
    private String tvquality;
    private String tvsize;
    tv(String tvquality , String tvsize ,String energyuse ,boolean garanti ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat ,int mojudi )
    {
        super(energyuse , garanti ,kalaid ,name ,brand ,price ,seller  , tozihat, mojudi);
        setTvquality(tvquality);
        setTvsize(tvsize);
    }
    void setTvsize(String tvsize) {this.tvsize = tvsize;}
    void setTvquality(String tvquality) {this.tvquality = tvquality;}
    public String getTvquality() {
        return tvquality;
    }
    public String getTvsize() {
        return tvsize;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "tv{" +
                "tvquality='" + tvquality + '\'' +
                ", tvsize='" + tvsize + '\'' +
                '}';
    }
}
class refregrator extends furniture
{
    private String capacity;
    private String refregeratortype;
    private boolean haverefregerator;
    refregrator(String capacity , String refregeratortype , boolean haverefregerator ,String energyuse ,boolean garanti ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat ,int mojudi )
    {
        super( energyuse , garanti , kalaid , name , brand , price , seller  , tozihat, mojudi );
        setCapacity(capacity);
        setHaverefregerator(haverefregerator);
        setRefregeratortype(refregeratortype);
    }
    void setCapacity(String capacity) {this.capacity = capacity;}
    void setRefregeratortype(String refregeratortype) {this.refregeratortype = refregeratortype;}
    void setHaverefregerator(boolean haverefregerator) {this.haverefregerator = haverefregerator;}

    public String getCapacity() {
        return capacity;
    }
    public String getRefregeratortype() {
        return refregeratortype;
    }

    @Override
    public String toString() {
        return   super.toString() +
                "refregrator{" +
                "capacity='" + capacity + '\'' +
                ", refregeratortype='" + refregeratortype + '\'' +
                ", haverefregerator=" + haverefregerator +
                '}';
    }
}
class gas extends furniture
{
    private int sholenum;
    private String gasmateryal;
    private boolean havingfer;
    gas(int sholenum , String gasmateryal , boolean havingfer ,String energyuse ,boolean garanti ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat  ,int mojudi)
    {
        super(energyuse , garanti , kalaid , name , brand , price , seller  , tozihat, mojudi );
        setGasmateryal(gasmateryal);
        setHavingfer(havingfer);
        setSholenum(sholenum);
    }
    void setSholenum(int sholenum)  {this.sholenum = sholenum;}
    void setGasmateryal(String gasmateryal) {this.gasmateryal = gasmateryal;}
    void setHavingfer (boolean havingfer) {this.havingfer = havingfer;}
    public int getSholenum() {
        return sholenum;
    }
    public String getGasmateryal() {
        return gasmateryal;
    }

    @Override
    public String toString() {
        return   super.toString() +
                "gas{" +
                "sholenum=" + sholenum +
                ", gasmateryal='" + gasmateryal + '\'' +
                ", havingfer=" + havingfer +
                '}';
    }
}
class eating extends kala
{
    private String expdate;
    private String toliddate;
    eating(String expdate , String toliddate ,long kalaid ,String name ,String brand ,long price ,String seller  ,String tozihat ,int mojudi)
    {
        super( kalaid , name , brand , price , seller  , tozihat ,  mojudi);
        setExpdate(expdate);
        setToliddate(toliddate);
    }
    void setExpdate(String expdate) {this.expdate = expdate;}
    void  setToliddate(String toliddate) { this.toliddate = toliddate;}
    public String getExpdate() {
        return expdate;
    }
    public String getToliddate() {
        return toliddate;
    }

    @Override
    public String toString() {
        return   super.toString() +
                "eating{" +
                "expdate='" + expdate + '\'' +
                ", toliddate='" + toliddate + '\'' +
                '}';
    }
}

//__________________________________________________________________________________________________________________

class rabetkarbari
{
    static int count = 0;
    static boolean jump;
    static void sortbycompareto()
    {
        for (int i = 0; i < listkala.kalalist.size(); ++i)
        {
            for (int j = 0; j < listkala.kalalist.size() - i - 1; ++j)
            {
                if (listkala.kalalist.get(j).compareTo(listkala.kalalist.get(j + 1)) == -1)
                {
                    kala swap = listkala.kalalist.get(j);
                    listkala.kalalist.set(j + 1, swap);
                }
            }
        }
    }
    static void showbaseinformation()
    {
        for (int k = 0; k < listkala.kalalist.size(); k++)
        {
            main.print(listkala.kalalist.get(k).toString());
        }
    }
    static void safheasli()
    {
        main.print("hi dear user! what do u wanna do ?");
        main.print("1.wanna go to my panel");
        main.print("2.wanna browse products" );
        int s = main.scanInt();
        switch(s)
        {
            case 1 :
                try
                {
                    mypanel();
                }
                catch (emailinvalid ei)
                {
                    main.print(ei.getMessage());
                }
                catch (invalidphonenum iph)
                {
                    main.print(iph.getMessage());
                }                    break;
            case 2:
                sortbycompareto();
                showbaseinformation();
                safheasli();
            default:
                main.print("please retry");
                safheasli();
        }
    }
    static void mypanel () throws emailinvalid , invalidphonenum
    {
        int index = 0;
        int iindex = 0;
        main.print( "what do u wanna do? ");
        main.print(" press 1 to log in into ur account");
        main.print("press 2 if u do not have an account");
        int s = main.scanInt();
        if (s == 1)
        {
            main.print(" which one is u ?");
            main.print("1.customer");
            main.print("2.seller");
            main.print("3.manager");
            int q = main.scanInt();
            if (q == 1)
            {
                main.print("please inter ur user name");
                String user = main.scanString();
                main.print(" please inter ur password ");
                String pass = main.scanString();
                jump = true;
                for ( int i = 0 ; i < customerlist.getCustomers().size() ; i++)
                {
                    if (customerlist.getCustomers().get(i).getUsername().equals(user) && customerlist.getCustomers().get(i).getPassword().equals(pass))
                    {
                        main.print(" u entred ur account successfuly");
                        jump = false;
                        iindex = i;
                        try
                        {
                            customermanager.customerpanel(iindex);
                        }
                        catch (invalidcharge ich)
                        {
                            main.print(ich.getMessage());
                        }
                        catch (invalidmojudi im)
                        {
                            im.getMessage();
                        }
                    }
                }
                if (jump == true)
                {
                    main.print("please retry");
                    mypanel();
                }
            }
            if (q == 2)
            {
                main.print("please inter ur user name");
                String suser = main.scanString();
                main.print(" please inter ur password ");
                String spass = main.scanString();
                jump = true;
                for ( int j = 0 ; j < sellerlist.getSellers().size() ; j++)
                {
                    if (sellerlist.getSellers().get(j).getUsername().equals(suser) && sellerlist.getSellers().get(j).getUsername().equals(spass))
                    {
                        jump = false;
                        index = j;
                        main.print(" u entred ur account successfuly");
                        try {
                            sellermanager.sellerpanel(index);
                        }
                        catch (InputMismatchException | invalidinput R)
                        {
                            main.print(R.getMessage());
                        }
                    }
                }
                if (jump == true)
                {
                    main.print("please retry");
                    mypanel();
                }
            }
            if (q == 3)
            {
                String manusername;
                String manpass;
                main.print("please inter ur user name");
                manusername = main.scanString();
                main.print("please inter ur pass");
                manpass = main.scanString();
                if ((manpass.equals("manager")) == true && (manusername.equals("manager")) == true)
                {
                    main.print("u entered ur account successfuly");
                    manager m1 = new manager();
                    managermanager.managerpanel();
                }
                else
                {
                    main.print("please retry");
                    mypanel();
                }
            }
        }

        if ( s == 2)
        {
            main.print(" which one u wanna sign up as ?");
            main.print(" 1.seller ");
            main.print("2.customer ");
            int k = main.scanInt();

            if (k == 1)
            {
                String fname;
                String flastname;
                String fusername;
                String femail;
                String fpassword;
                long fphonenumber;
                String factoryname;

                main.print("please inter ur name");
                fname = main.scanString();
                main.print("please inter ur last name");
                flastname = main.scanString();
                main.print("please inter ur user name");
                fusername = main.scanString();
                main.print("please inter ur email");
                femail = main.scanString();
                String femailregix = "[A - Za-z0-9+-.]+@(.+)$";
                Pattern patternFemail = Pattern.compile(femailregix);
                Matcher matcher = patternFemail.matcher(femail);
                if (!(matcher.matches()))
                {
                    throw new emailinvalid();
                }
                main.print("please inter ur password");
                fpassword = main.scanString();
                main.print("please inter ur phone number");
                fphonenumber = main.scanLong();
                if (fphonenumber > 4259999)
                {
                    throw new invalidphonenum();
                }
                main.print("please inter ur factory name");
                factoryname = main.scanString();
                main.print("charge");
                int fcharge = main.scanInt();
                seller s1 = new seller(fname, flastname, fusername, femail, fpassword, fphonenumber, factoryname,fcharge);
                requestlist.waitingsellers.add(s1);
                main.print("we submited ur request.please wait for manager request");
                safheasli();
            }
            if ( k == 2)
            {
                String name;
                String lastname;
                String username;
                String email;
                String password;
                long phonenumber;

                main.print("please inter ur name");
                name = main.scanString();
                main.print("please inter ur last name");
                lastname = main.scanString();
                main.print("please inter ur user name");
                username = main.scanString();
                main.print("please inter ur email");
                email = main.scanString();
                String femailregix = "[A - Za-z0-9+-.]+@(.+)$";
                Pattern patternFemail = Pattern.compile(femailregix);
                Matcher matcher = patternFemail.matcher(email);
                if (!(matcher.matches()))
                {
                    throw new emailinvalid();
                }
                main.print("please inter ur password");
                password = main.scanString();
                main.print("please inter ur phone number");
                phonenumber = main.scanLong();
                if (phonenumber > 4259999)
                {
                    throw new invalidphonenum();
                }
                main.print("please inter ur phone number");
                phonenumber = main.scanLong();
                main.print("charge");
                int ccharge = main.scanInt();
                customer c1 = new customer(name, lastname, username, email, password, phonenumber,ccharge);
                customerlist.signupcustomer(c1);
                main.print("we submited ur sign up.now please log in into ur account");
                mypanel();
            }
        }   }

}
class sellermanager
{

    static boolean havingfer = true;
    static boolean gaming = true;
    static boolean garanti = true;
    static boolean havingrefregerator = true;
    manager m1;
    public static int kalaid = 0;

    static void changeseller(int index ,String name, String lastname,String email,String password,long phonenumber,String factoryname )
    {
        sellerlist.getSellers().get(index).setName(name);
        sellerlist.getSellers().get(index).setLastname(lastname);
        sellerlist.getSellers().get(index).setPassword(password);
        sellerlist.getSellers().get(index).setPhonenumber(phonenumber);
        sellerlist.getSellers().get(index).setFactoryname(factoryname);
        sellerlist.getSellers().get(index).setEmail(email);
    }
    static void shopping()
    {
        main.print("please inter the kalaid of your product");
        int id = main.scanInt();
        for (int vb = 0 ; vb < id ; vb ++)
        {
            if (id == listkala.kalalist.get(vb).getKalaid())
                listkala.sabadekala.add(listkala.kalalist.get(vb));
            main.print("this product was added to ur sabade kharid successfuly");
        }
    }
    static void sellerpanel(int i) throws invalidinput
    {

        Scanner scanner = new Scanner(System.in);
        main.print("what can i do for 4");
        main.print("1.change my personal info");
        main.print("2.i wanna add a new product ");
        main.print("3.i wanna edit a product ");
        main.print("4.i wanna remove a product");
        main.print("7.see my information");
        main.print("5.wanna log out");

        int t = main.scanInt();
        if( t == 1 )
        {
            main.print("please inter ur information again");
            main.print("name");
            String name = main.scanString();
            main.print("last name");
            String lastname = main.scanString();
            main.print("password");
            String password = main.scanString();
            main.print("phone number");
            Long phone = main.scanLong();
            main.print("email");
            String email = main.scanString();
            main.print("factory name");
            String factory = main.scanString();
            changeseller( i , name,  lastname, email, password, phone, factory );
            sellerpanel(i);
        }
        if( t == 2 )
        {
            main.print("in which category is ur product ?");
            main.print("1.digital");
            main.print("2.pooshak");
            main.print("3.furniture");
            main.print("4.eating");
            int y =main.scanInt();
            if ( y == 1 )
            {
                main.print("which one is ur product ?");
                main.print("1.mobile");
                main.print("2.laptop");
                int yy = main.scanInt();
                if ( yy == 2)
                {
                    main.print("please enter the required information");
                    main.print(" is ur laptop gaming ?            1-yes     2-no");
                    int ee = 0;
                    try
                    {
                        ee = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }

                    switch (ee)
                    {
                        case 1 :
                            gaming = true;
                            break;
                        case 2 :
                            gaming = false;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    main.print("pardazande :");
                    String pardazande = main.scanString();
                    //_____________________________
                    main.print("memory :");
                    int memory = 0;
                    try {
                        memory = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("ram :");
                    int ram = 0;
                    try {
                        ram = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("systemamel :");
                    String systemamel = main.scanString();
                    main.print("weigh :");
                    int weigh = 0;
                    try {
                        weigh = main.scanInt();

                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("size :");
                    String size = main.scanString();
                    //______________________________
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();
                    //________________________________
                    laptop lap = new laptop( pardazande,  gaming , memory , ram , systemamel , weigh , size , kalaid , name , brand , price , seller  , tozihat, mojudii);
                    addkalarequest.addrequest.add(lap);
                    klaptop.laptoplist.add(lap);
                    sellerpanel(i);
                }
                if ( yy == 1 )
                {

                    main.print("simnum :");
                    int simnum = 0;
                    try {
                        simnum = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("camquality :");
                    int  camquality = 0;
                    try {
                        camquality = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    //_____________________________
                    main.print("memory :");
                    int memory = 0;
                    try {
                        memory = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("ram :");
                    int ram =0;
                    try {
                        ram = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("systemamel :");
                    String systemamel = main.scanString();
                    main.print("weigh :");
                    int weigh = 0;
                    try {
                        weigh = main.scanInt();
                    }
                    catch (InputMismatchException ii)
                    {
                        main.print(ii.toString());
                    }
                    main.print("size :");
                    String size = main.scanString();
                    //______________________________
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();
                    //________________________________
                    mobile mob = new mobile(camquality, simnum ,memory ,  ram , systemamel ,weigh ,size , kalaid , name , brand , price , seller , tozihat, mojudii);
                    addkalarequest.addrequest.add(mob);
                    kmobile.mobilelist.add(mob);
                    sellerpanel(i);
                }
            }
            if ( y == 2 )
            {
                main.print("which one is ur product ?");
                main.print("1.lebas");
                main.print("2.shoes");
                int yy = main.scanInt();

                if ( yy == 2)
                {
                    main.print("size");
                    int sizek = main.scanInt();
                    //_____________________________
                    main.print("materyal :");
                    String materyal = main.scanString();
                    main.print(" made in : ");
                    String madein = main.scanString();
                    //______________________________
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();
                    main.print("shoe type : ");
                    String type = main.scanString();
                    //________________________________
                    shoes shoe = new shoes(  sizek , shoes.shoestype.valueOf(type) ,  materyal , madein , kalaid , name , brand , price , seller, tozihat, mojudii);
                    addkalarequest.addrequest.add(shoe);
                    kshoes.shoeslist.add(shoe);
                    sellerpanel(i);

                }
                if ( yy == 1 )
                {
                    main.print("size");
                    String lsize = main.scanString();
                    //____________________________
                    main.print("materyal :");
                    String materyal = main.scanString();
                    main.print(" made in : ");
                    String madein = main.scanString();
                    //______________________________
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();
                    main.print("tanavo of product : ");
                    String tanavo = main.scanString();
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    //________________________________
                    lebas dress = new lebas( lsize , lebas.tanavo.valueOf(tanavo) ,  materyal ,madein , kalaid , name , brand , price ,seller, tozihat, mojudii);
                    addkalarequest.addrequest.add(dress);
                    klebas.lebaslist.add(dress);
                    main.print(dress.getVarious().name());
                    sellerpanel(i);

                }
            }
            if ( y == 3 )
            {
                main.print("which one is ur product ?");
                main.print("1.gas");
                main.print("2.tv");
                main.print("3.refregerator");
                int yy = main.scanInt();

                if ( yy == 1)
                {
                    main.print(" does this gas have garanti ?            1-yes     2-no");
                    int ee = main.scanInt();
                    switch (ee)
                    {
                        case 1 :
                            garanti = true;
                            break;
                        case 2 :
                            garanti = false;
                            break;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    main.print("energy use");
                    String energyuse = main.scanString();
                    main.print("shole number");
                    int sholenum = main.scanInt();
                    main.print("gasmateryal");
                    String gasmateryal = main.scanString();
                    main.print(" does this gas have fer ?            1-yes     2-no");
                    int eie = main.scanInt();
                    switch (eie)
                    {
                        case 1 :
                            havingfer = true;
                            break;
                        case 2 :
                            havingfer = false;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    //______________________________
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();
                    //________________________________
                    gas gaz = new gas( sholenum , gasmateryal , havingfer , energyuse , garanti , kalaid , name , brand , price , seller, tozihat, mojudii);
                    addkalarequest.addrequest.add(gaz);
                    kgas.gaslist.add(gaz);
                }
                if ( yy == 2 )
                {

                    main.print("tv quality ");
                    String tvquality = main.scanString();
                    main.print("tv size");
                    String tvsize = main.scanString();
                    //---------------------------------------
                    main.print(" does this tv have garanti ?            1-yes     2-no");
                    int ee = main.scanInt();
                    switch (ee)
                    {
                        case 1 :
                            garanti = true;
                            break;
                        case 2 :
                            garanti = false;
                            break;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    main.print("energy use");
                    String energyuse = main.scanString();
                    //______________________________
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();

                    //________________________________
                    tv televesion = new tv( tvquality , tvsize , energyuse , garanti, kalaid , name , brand , price ,seller, tozihat, mojudii);
                    addkalarequest.addrequest.add(televesion);
                    ktv.tvlist.add(televesion);
                }
                if (yy == 3)
                {
                    //_________________________________________
                    main.print(" does this refregerator have garanti ?   1-yes     2-no");
                    int ee = main.scanInt();
                    switch (ee)
                    {
                        case 1 :
                            garanti = true;
                            break;
                        case 2 :
                            garanti = false;
                            break;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    main.print("energy use");
                    String energyuse = main.scanString();
                    main.print("how many do u have from this product");
                    int mojudii = main.scanInt();
                    //______________________________
                    main.print(" does this refregerator have refregerator ?   1-yes     2-no");
                    int eue = main.scanInt();
                    switch (eue)
                    {
                        case 1 :
                            havingrefregerator = true;
                            break;
                        case 2 :
                            havingrefregerator = false;
                            break;
                        default:
                            main.print("try again");
                            sellerpanel(i);
                    }
                    main.print("capacity");
                    String capacity = main.scanString();
                    main.print("refregeratortype");
                    String refregeratortype = main.scanString();
                    //______________________________
                    kalaid = kalaid + 1;
                    String seller = sellerlist.getSellers().get(i).getName();
                    main.print("name of product");
                    String name = main.scanString();
                    main.print("brand of product :");
                    String brand = main.scanString();
                    main.print("price of product :");
                    long price = main.scanLong();
                    main.print("tozihat of product :");
                    String tozihat = main.scanString();

                    //________________________________
                    refregrator ref = new refregrator(capacity , refregeratortype , havingrefregerator  ,energyuse , garanti, kalaid , name , brand , price ,seller, tozihat, mojudii);
                    addkalarequest.addrequest.add(ref);
                    kref.reflist.add(ref);
                }
            }
            if ( y == 4 )
            {
                main.print("tolid date");
                String toliddate = main.scanString();
                main.print("expdate");
                String expdate = main.scanString();
                //______________________________
                kalaid = kalaid + 1;
                String seller = sellerlist.getSellers().get(i).getName();
                main.print("name of product");
                String name = main.scanString();
                main.print("brand of product :");
                String brand = main.scanString();
                main.print("price of product :");
                long price = main.scanLong();
                main.print("tozihat of product :");
                String tozihat = main.scanString();
                main.print("how many do u have from this product");
                int mojudii = main.scanInt();
                //________________________________
                eating eat = new eating( expdate ,  toliddate, kalaid , name , brand , price , seller  , tozihat , mojudii);
                addkalarequest.addrequest.add(eat);
                keating.eatlist.add(eat);
            }
        }
        if( t == 5)
        {
            main.print("u logged out");
            rabetkarbari.safheasli();
        }
        if (t == 7)
            main.print(sellerlist.getSellers().get(i).getName());
        main.print(sellerlist.getSellers().get(i).getLastname());
        main.print(sellerlist.getSellers().get(i).getEmail());
        main.print(sellerlist.getSellers().get(i).getPassword());
        main.print(sellerlist.getSellers().get(i).getPhonenumber());
        main.print(sellerlist.getSellers().get(i).getUsername());
        main.print(sellerlist.getSellers().get(i).getFactoryname());
        sellerpanel(i);
    }
}
class customermanager
{
    static void gotodefult()
    {
        if (fasl.faasl.equals(1))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof mobile || listkala.kalalist.get(rr)instanceof laptop)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()*3);
            }
        }
        if (fasl.faasl.equals(2))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof lebas || listkala.kalalist.get(rr)instanceof shoes )
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()*3);
            }
        }
        if (fasl.faasl.equals(3))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof refregrator || listkala.kalalist.get(rr)instanceof tv || listkala.kalalist.get(rr)instanceof gas)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()*3);
            }
        }
        if (fasl.faasl.equals(4))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof eating)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()*3);
            }
        }
    }
    static void changeseason()
    {
        if (fasl.faasl.equals(1))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof mobile || listkala.kalalist.get(rr)instanceof laptop)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()/3);
            }
        }
        if (fasl.faasl.equals(2))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof lebas || listkala.kalalist.get(rr)instanceof shoes )
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()/3);
            }
        }
        if (fasl.faasl.equals(3))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof refregrator || listkala.kalalist.get(rr)instanceof tv || listkala.kalalist.get(rr)instanceof gas)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()/3);
            }
        }
        if (fasl.faasl.equals(4))
        {
            for (int rr = 0 ; rr < listkala.kalalist.size(); rr++)
            {
                if (listkala.kalalist.get(rr)instanceof eating)
                    listkala.kalalist.get(rr).setPrice(listkala.kalalist.get(rr).getPrice()/3);
            }
        }
    }
    static void showkala(int inn)
    {
        changeseason();
        main.print("1.i wanna see all products with all categories");
        main.print("2.i wanna see a specific product");
        main.print("3.i wanna search a product by its name");
        int a = main.scanInt();
        if (a == 1)
        {
            rabetkarbari.sortbycompareto();
            rabetkarbari.showbaseinformation();
        }
        if (a == 3)
        {
            main.print("please inter the name of product");
            String nameof = main.scanString();
            for (int jj = 0 ; jj < listkala.kalalist.size(); jj++)
            {
                if (listkala.kalalist.get(jj).getName().equals(nameof))
                    main.print(listkala.kalalist.get(jj).toString());
            }
            gotodefult();
            try
            {
                customerpanel(inn);
            }
            catch (invalidcharge ich)
            {
                main.print(ich.getMessage());
            }
            catch (invalidmojudi im)
            {
                im.getMessage();
            }            }
        if (a == 1)
        {
            rabetkarbari.sortbycompareto();
            rabetkarbari.showbaseinformation();
            try
            {
                customerpanel(inn);
            }
            catch (invalidcharge ich)
            {
                main.print(ich.getMessage());
            }
            catch (invalidmojudi im)
            {
                im.getMessage();
            }                gotodefult();
        }
        if (a == 2)
        {
            main.print("which category you wanna see ? ");
            main.print("1.digital");
            main.print("2.furniture");
            main.print("3.pooshak");
            main.print("4.eating");
            int ae = main.scanInt();
            if (ae == 1)
            {
                main.print("1.mobile");
                main.print("2.laptop");
                int ea = main.scanInt();
                if (ea == 1)
                {
                    main.print("1.i wanna see all the mobiles");
                    main.print("2.i wanna filter this product");
                    int mm = main.scanInt();
                    if (mm == 1)
                    {
                        for (int p = 0 ; p < kmobile.mobilelist.size() ; p++)
                        {
                            main.print(kmobile.mobilelist.get(p).toString());
                        }
                        gotodefult();
                        try
                        {
                            customerpanel(inn);
                        }
                        catch (invalidcharge ich)
                        {
                            main.print(ich.getMessage());
                        }
                        catch (invalidmojudi im)
                        {
                            im.getMessage();
                        }
                    }
                    if (mm == 2)
                    {
                        main.print("please inter the min price");
                        int min = main.scanInt();
                        main.print("please inter the max price");
                        int max = main.scanInt();
                        main.print("press 1 to see all products or press 2 to see available products");
                        int available = main.scanInt();
                        main.print("please inter the capacity of ram");
                        int ram = main.scanInt();
                        main.print("please inter the number of simcard");
                        int simnum = main.scanInt();
                        if (available == 1)
                        {
                            for (int p = 0; p < kmobile.mobilelist.size(); p++)
                            {
                                if (kmobile.mobilelist.get(p).getPrice() >= min && kmobile.mobilelist.get(p).getPrice() <= max && kmobile.mobilelist.get(p).getSimcardnum() == simnum && kmobile.mobilelist.get(p).getRam() == ram)
                                {
                                    main.print(kmobile.mobilelist.get(p).toString());
                                }
                            }
                        }
                        if (available == 2)
                        {
                            for (int p = 0; p < kmobile.mobilelist.size(); p++)
                            {
                                if (kmobile.mobilelist.get(p).getPrice() >= min && kmobile.mobilelist.get(p).getPrice() <= max && kmobile.mobilelist.get(p).getSimcardnum() == simnum && kmobile.mobilelist.get(p).getRam() == ram &&kmobile.mobilelist.get(p).getMojudi() > 0)
                                {
                                    main.print(kmobile.mobilelist.get(p).toString());
                                }
                            }
                        }
                    }
                    gotodefult();
                    try
                    {
                        customerpanel(inn);
                    }
                    catch (invalidcharge ich)
                    {
                        main.print(ich.getMessage());
                    }
                    catch (invalidmojudi im)
                    {
                        im.getMessage();
                    }                    }
                if (ea == 2)
                {
                    main.print("1.i wanna see all the laptops");
                    main.print("2.i wanna filter this product");
                    int mm = main.scanInt();
                    if (mm == 1)
                    {
                        for (int p = 0 ; p < klaptop.laptoplist.size() ; p++)
                        {
                            main.print(klaptop.laptoplist.get(p).toString());
                        }
                    }
                    if (mm == 2)
                    {
                        main.print("please inter the min price");
                        int min = main.scanInt();
                        main.print("please inter the max price");
                        int max = main.scanInt();
                        main.print("press 1 to see all products or press 2 to see available products");
                        int available = main.scanInt();
                        main.print("please inter the capacity of ram");
                        int ram = main.scanInt();
                        main.print("please inter the type of pardazande");
                        String pardazande = main.scanString();
                        if (available == 1)
                        {
                            for (int p = 0; p < klaptop.laptoplist.size(); p++)
                            {
                                if (klaptop.laptoplist.get(p).getPrice() >= min && klaptop.laptoplist.get(p).getPrice() <= max && klaptop.laptoplist.get(p).getPardazande().equals(pardazande) && klaptop.laptoplist.get(p).getRam() == ram)
                                {
                                    main.print(klaptop.laptoplist.get(p).toString());
                                }
                            }
                        }
                        if (available == 2)
                        {
                            for (int p = 0; p < klaptop.laptoplist.size(); p++)
                            {
                                if (klaptop.laptoplist.get(p).getPrice() >= min && klaptop.laptoplist.get(p).getPrice() <= max && klaptop.laptoplist.get(p).getPardazande().equals(pardazande) && klaptop.laptoplist.get(p).getRam() == ram)
                                {
                                    main.print(klaptop.laptoplist.get(p).toString());
                                }
                            }
                        }
                    }
                    gotodefult();
                    try
                    {
                        customerpanel(inn);
                    }
                    catch (invalidcharge ich)
                    {
                        main.print(ich.getMessage());
                    }
                    catch (invalidmojudi im)
                    {
                        im.getMessage();
                    }                    }
                if (ae == 2)
                {
                    main.print("1.gas");
                    main.print("2.tv");
                    main.print("3.refregerator");
                    int ee = main.scanInt();
                    if (ee == 1)
                    {
                        main.print("1.i wanna see all the gases");
                        main.print("2.i wanna filter this gas");
                        int mm = main.scanInt();
                        if (mm == 1)
                        {
                            for (int p = 0 ; p < kgas.gaslist.size() ; p++)
                            {
                                main.print(kgas.gaslist.get(p).toString());
                            }
                        }
                        if (mm == 2)
                        {
                            main.print("please inter the min price");
                            int min = main.scanInt();
                            main.print("please inter the max price");
                            int max = main.scanInt();
                            main.print("press 1 to see all products or press 2 to see available products");
                            int available = main.scanInt();
                            main.print("inter energy use");
                            String energy = main.scanString();
                            main.print("number of shole");
                            int shole = main.scanInt();
                            if (available == 1)
                            {
                                for (int p = 0; p < kgas.gaslist.size(); p++)
                                {
                                    if (kgas.gaslist.get(p).getPrice() >= min && kgas.gaslist.get(p).getPrice() <= max && kgas.gaslist.get(p).getEnergyuse().equals(energy) && kgas.gaslist.get(p).getSholenum() == shole )
                                    {
                                        main.print(kgas.gaslist.get(p).toString());
                                    }
                                }
                            }
                            if (available == 2)
                            {
                                for (int p = 0; p < kgas.gaslist.size(); p++)
                                {
                                    if (kgas.gaslist.get(p).getPrice() >= min && kgas.gaslist.get(p).getPrice() <= max && kgas.gaslist.get(p).getEnergyuse().equals(energy) && kgas.gaslist.get(p).getSholenum() == shole && kgas.gaslist.get(p).getMojudi() > 0 )
                                    {
                                        main.print(kgas.gaslist.get(p).toString());
                                    }
                                }
                            }
                            gotodefult();
                            try
                            {
                                customerpanel(inn);
                            }
                            catch (invalidcharge ich)
                            {
                                main.print(ich.getMessage());
                            }
                            catch (invalidmojudi im)
                            {
                                im.getMessage();
                            }                            }
                        if (ee == 2)
                        {
                            main.print("1.i wanna see all the tv");
                            main.print("2.i wanna filter this product");
                            int mmm = main.scanInt();
                            if (mmm == 1)
                            {
                                for (int p = 0 ; p < ktv.tvlist.size() ; p++)
                                {
                                    main.print(ktv.tvlist.get(p).toString());
                                }
                            }
                            if (mmm == 2)
                            {
                                main.print("please inter the min price");
                                int min = main.scanInt();
                                main.print("please inter the max price");
                                int max = main.scanInt();
                                main.print("press 1 to see all products or press 2 to see available products");
                                int available = main.scanInt();
                                main.print("inter energy use");
                                String energy = main.scanString();
                                main.print("tv quality");
                                String quality = main.scanString();
                                if (available == 1)
                                {
                                    for (int p = 0; p < ktv.tvlist.size(); p++)
                                    {
                                        if (ktv.tvlist.get(p).getPrice() >= min && ktv.tvlist.get(p).getPrice() <= max && ktv.tvlist.get(p).getEnergyuse().equals(energy) && ktv.tvlist.get(p).getTvquality().equals(quality) )
                                        {
                                            main.print(ktv.tvlist.get(p).toString());
                                        }
                                    }
                                }
                                if (available == 2)
                                {
                                    for (int p = 0; p < ktv.tvlist.size(); p++)
                                    {
                                        if (ktv.tvlist.get(p).getPrice() >= min && ktv.tvlist.get(p).getPrice() <= max && ktv.tvlist.get(p).getEnergyuse().equals(energy) && ktv.tvlist.get(p).getTvquality().equals(quality) && ktv.tvlist.get(p).getMojudi() >0 )
                                        {
                                            main.print(ktv.tvlist.get(p).toString());
                                        }
                                    }
                                }
                                gotodefult();
                                try
                                {
                                    customerpanel(inn);
                                }
                                catch (invalidcharge ich)
                                {
                                    main.print(ich.getMessage());
                                }
                                catch (invalidmojudi im)
                                {
                                    im.getMessage();
                                }                                }

                            if (ee == 3)
                            {
                                main.print("1.i wanna see all the refregerator");
                                main.print("2.i wanna filter this product");
                                int mmmm = main.scanInt();
                                if (mmmm == 1)
                                {
                                    for (int p = 0 ; p < kref.reflist.size() ; p++)
                                    {
                                        main.print(kref.reflist.get(p).toString());
                                    }
                                }
                                if (mmmm == 2)
                                {
                                    main.print("please inter the min price");
                                    int min = main.scanInt();
                                    main.print("please inter the max price");
                                    int max = main.scanInt();
                                    main.print("press 1 to see all products or press 2 to see available products");
                                    int available = main.scanInt();
                                    main.print("inter energy use");
                                    String energy = main.scanString();
                                    main.print("capacity");
                                    String capacity = main.scanString();
                                    if (available == 1)
                                    {
                                        for (int p = 0; p < kref.reflist.size(); p++)
                                        {
                                            if (kref.reflist.get(p).getPrice() >= min && kref.reflist.get(p).getPrice() <= max && kref.reflist.get(p).getEnergyuse().equals(energy) && kref.reflist.get(p).getCapacity().equals(capacity)  )
                                            {
                                                main.print(kref.reflist.get(p).toString());
                                            }
                                        }
                                    }
                                    if (available == 2)
                                    {
                                        for (int p = 0; p < kref.reflist.size(); p++)
                                        {
                                            if (kref.reflist.get(p).getPrice() >= min && kref.reflist.get(p).getPrice() <= max && kref.reflist.get(p).getEnergyuse().equals(energy) && kref.reflist.get(p).getCapacity().equals(capacity) && kref.reflist.get(p).getMojudi() > 0  )
                                            {
                                                main.print(kref.reflist.get(p).toString());
                                            }
                                        }
                                    }
                                }
                            }
                            gotodefult();
                            try
                            {
                                customerpanel(inn);
                            }
                            catch (invalidcharge ich)
                            {
                                main.print(ich.getMessage());
                            }
                            catch (invalidmojudi im)
                            {
                                im.getMessage();
                            }                            }
                        if (ae == 3)
                        {
                            main.print("1.lebas");
                            main.print("2.shoes");
                            int eee = main.scanInt();
                            if (eee == 1)
                            {
                                main.print("1.i wanna see all the lebas");
                                main.print("2.i wanna filter this product");
                                int mom = main.scanInt();
                                if (mom == 1)
                                {
                                    for (int p = 0 ; p < klebas.lebaslist.size() ; p++)
                                    {
                                        main.print( klebas.lebaslist.get(p).toString());
                                    }
                                }
                                if (mom == 2)
                                {
                                    main.print("please inter the min price");
                                    int min = main.scanInt();
                                    main.print("please inter the max price");
                                    int max = main.scanInt();
                                    main.print("press 1 to see all products or press 2 to see available products");
                                    int available = main.scanInt();
                                    main.print("materyal");
                                    String materyal = main.scanString();
                                    main.print("size of lebas");
                                    String sizel = main.scanString();
                                    if (available == 1)
                                    {
                                        for (int p = 0; p < klebas.lebaslist.size(); p++)
                                        {
                                            if (klebas.lebaslist.get(p).getPrice() >= min && klebas.lebaslist.get(p).getPrice() <= max && klebas.lebaslist.get(p).getMateryal().equals(materyal) && klebas.lebaslist.get(p).getLsize().equals(sizel)  )
                                            {
                                                main.print(klebas.lebaslist.get(p).toString());
                                            }
                                        }
                                    }
                                    if (available == 2)
                                    {
                                        for (int p = 0; p < klebas.lebaslist.size(); p++)
                                        {
                                            if (klebas.lebaslist.get(p).getPrice() >= min && klebas.lebaslist.get(p).getPrice() <= max && klebas.lebaslist.get(p).getMateryal().equals(materyal) && klebas.lebaslist.get(p).getLsize().equals(sizel) &&klebas.lebaslist.get(p).getMojudi() > 0 )
                                            {
                                                main.print(klebas.lebaslist.get(p).toString());
                                            }
                                        }
                                    }
                                }
                                gotodefult();
                                try
                                {
                                    customerpanel(inn);
                                }
                                catch (invalidcharge ich)
                                {
                                    main.print(ich.getMessage());
                                }
                                catch (invalidmojudi im)
                                {
                                    im.getMessage();
                                }                                }
                            if (eee == 2)
                            {
                                main.print("1.i wanna see all the shoes");
                                main.print("2.i wanna filter this product");
                                int mom = main.scanInt();
                                if (mom == 1)
                                {
                                    for (int p = 0 ; p < kshoes.shoeslist.size() ; p++)
                                    {
                                        main.print( kshoes.shoeslist.get(p).toString());
                                    }
                                }
                                if (mom == 2)
                                {

                                    main.print("please inter the min price");
                                    int min = main.scanInt();
                                    main.print("please inter the max price");
                                    int max = main.scanInt();
                                    main.print("press 1 to see all products or press 2 to see available products");
                                    int available = main.scanInt();
                                    main.print("materyal");
                                    String materyal = main.scanString();
                                    main.print("size of lebas");
                                    int sezek = main.scanInt();
                                    if (available == 1)
                                    {
                                        for (int p = 0; p < kshoes.shoeslist.size(); p++)
                                        {
                                            if (kshoes.shoeslist.get(p).getPrice() >= min && kshoes.shoeslist.get(p).getPrice() <= max && kshoes.shoeslist.get(p).getMateryal().equals(materyal) && kshoes.shoeslist.get(p).getSizek() == sezek  )
                                            {
                                                main.print(kshoes.shoeslist.get(p).toString());
                                            }
                                        }
                                    }
                                    if (available == 2)
                                    {
                                        for (int p = 0; p < kshoes.shoeslist.size(); p++)
                                        {
                                            if (kshoes.shoeslist.get(p).getPrice() >= min && kshoes.shoeslist.get(p).getPrice() <= max && kshoes.shoeslist.get(p).getMateryal().equals(materyal) && kshoes.shoeslist.get(p).getSizek() == sezek && kshoes.shoeslist.get(p).getMojudi() > 0 )
                                            {
                                                main.print(kshoes.shoeslist.get(p).toString());
                                            }
                                        }
                                    }

                                }
                            }
                            gotodefult();
                            try
                            {
                                customerpanel(inn);
                            }
                            catch (invalidcharge ich)
                            {
                                main.print(ich.getMessage());
                            }
                            catch (invalidmojudi im)
                            {
                                im.getMessage();
                            }                            }
                        if (ae == 4)
                        {
                            main.print("1.i wanna see all the eat staff");
                            main.print("2.i wanna filter this product");
                            int mom = main.scanInt();
                            if (mom == 1)
                            {
                                for (int p = 0 ; p < keating.eatlist.size() ; p++)
                                {
                                    main.print( keating.eatlist.get(p).toString());
                                }
                            }
                            if (mom == 2)
                            {
                                main.print("please inter the min price");
                                int min = main.scanInt();
                                main.print("please inter the max price");
                                int max = main.scanInt();
                                main.print("press 1 to see all products or press 2 to see available products");
                                int available = main.scanInt();
                                if (available == 1)
                                {
                                    for (int p = 0; p < keating.eatlist.size(); p++)
                                    {
                                        if (keating.eatlist.get(p).getPrice() >= min && keating.eatlist.get(p).getPrice() <= max )
                                        {
                                            main.print(keating.eatlist.get(p).toString());
                                        }
                                    }
                                }
                                if (available == 2)
                                {
                                    for (int p = 0; p < keating.eatlist.size(); p++)
                                    {
                                        if (keating.eatlist.get(p).getPrice() >= min && keating.eatlist.get(p).getPrice() <= max && keating.eatlist.get(p).getMojudi() > 0 )
                                        {
                                            main.print(keating.eatlist.get(p).toString());
                                        }
                                    }
                                }
                                gotodefult();
                                try
                                {
                                    customerpanel(inn);
                                }
                                catch (invalidcharge ich)
                                {
                                    main.print(ich.getMessage());
                                }
                                catch (invalidmojudi im)
                                {
                                    im.getMessage();
                                }                                }
                        }
                    }
                }
            }
        }
        try
        {
            customerpanel(inn);
        }
        catch (invalidcharge ich)
        {
            main.print(ich.getMessage());
        }
        catch (invalidmojudi im)
        {
            im.getMessage();
        }

    }

    static void changecustomer(int iindex ,String name, String lastname,String email,String password,long phonenumber)
    {
        customerlist.getCustomers().get(iindex).setName(name);
        customerlist.getCustomers().get(iindex).setEmail(email);
        customerlist.getCustomers().get(iindex).setPassword(password);
        customerlist.getCustomers().get(iindex).setPhonenumber(phonenumber);
        customerlist.getCustomers().get(iindex).setLastname(lastname);
    }
    static void customerpanel(int j) throws invalidmojudi , invalidcharge
    {
        Scanner scanner = new Scanner(System.in);
        main.print("what can i do for 4");
        main.print("1.change my personal info");
        main.print("2.wanna browse products");
        main.print("3.wanna log out");
        main.print("4.i wanna buy some products");
        main.print("5.i wanna look at my sabadekharid");
        main.print("6.i wanna get an off code");
        int t = main.scanInt();
        switch (t)
        {
            case 1 :
                main.print("name");
                String name = main.scanString();
                main.print("last name");
                String lastname = main.scanString();
                main.print("password");
                String password = main.scanString();
                main.print("phone number");
                Long phone = main.scanLong();
                main.print("email");
                String email = main.scanString();
                changecustomer( j , name,  lastname, email, password, phone);
                customerpanel(j);

            case 2 :
                showkala(j);
            case 3 :
                try
                {
                    rabetkarbari.mypanel();
                }
                catch (emailinvalid ei)
                {
                    main.print(ei.getMessage());
                }
                catch (invalidphonenum iph)
                {
                    main.print(iph.getMessage());
                }
                customerpanel(j);
            case 4 :
                main.print("pleas inter the the id of product you wanna buy");
                for (int kk = 0 ;  ; kk++)
                {
                    int in = main.scanInt();
                    listkala.sabadekala.add(listkala.kalalist.get(kk));
                    if (listkala.sabadekala.get(kk).getMojudi() < 1)
                    {
                        throw new invalidmojudi();
                    }
                    main.print("do you want to add on another product?     1.yes    2.no");
                    int co = main.scanInt();
                    if (co == 1)
                        continue;
                    else
                        break;
                }
                customerpanel(j);

            case 5 :
                for (int jh = 0 ; jh < listkala.sabadekala.size() ; jh++)
                    main.print(listkala.sabadekala.get(jh).toString());
                main.print("press 1 to remove a product");
                main.print("press 2 to complete your shopping");
                main.print("press 3 to back to main menu");
                int buy = main.scanInt();
                if (buy == 2)
                    buy(j);
                customerpanel(j);
            case 6 :
                main.print("this is ur off code!");
                String takhfif = RandomString.getAlphaNumericString();
                customerlist.customers.get(j).setOffcode(takhfif);
                main.print(takhfif);
                customerpanel(j);
            default:
                customerpanel(j);
        }
    }
    static void buy(int u)
    {
        changeseason();
        long total = 0;
        main.print("this is the price of your products before off");
        for (int ty  = 0 ; ty < listkala.sabadekala.size() ; ty ++)
            total = listkala.sabadekala.get(ty).getPrice() + total;
        main.print(total);
        main.print("press?     1.yes   2.no" );
        int uu = main.scanInt();
        if (uu == 1)
            main.print("now inter ur off code");
        String coode = main.scanString();
        if (customerlist.customers.get(u).getOffcode().equals(coode))
        {
            main.print("this is the price after off");
            total = total/2;
            main.print(total);
        }
        main.print("press 3 to lead to bank link");
        int ee = main.scanInt();
        if (ee == 3)
        {
            if (customerlist.getCustomers().get(u).getCharge() < total)
                try
                {
                    throw new invalidcharge();
                }
                catch (invalidcharge ich)
                {
                    main.print(ich.getMessage());
                }

        }
        gotodefult();
    }
}
class managermanager
{
    static void managerpanel()
    {
        main.print("what do u wanna do");
        main.print("1.change my personal information");
        main.print("2.wanna see sellers sign in  requests");
        main.print("3.wanna remove a member");
        main.print("4.wanna take a look at java kala members");
        main.print("5.wanna log out");
        main.print("6.wanna see add product requests");
        int z = main.scanInt();
        if (z == 1)
        {
        }
        if (z == 2)
        {
            for (int u = 0; u < requestlist.waitingsellers.size() ; u++)
            {
                main.print("information of " + (u + 1) + " th seller");
                main.print(requestlist.getWaitingsellers().get(u).getName());
                main.print(requestlist.getWaitingsellers().get(u).getLastname());
                main.print(requestlist.getWaitingsellers().get(u).getEmail());
                main.print(requestlist.getWaitingsellers().get(u).getPassword());
                main.print(requestlist.getWaitingsellers().get(u).getUsername());
                main.print(requestlist.getWaitingsellers().get(u).getPhonenumber());
                main.print(requestlist.getWaitingsellers().get(u).getFactoryname());
                main.print("");
                main.print("");
                main.print("press 1 to decline the " + (u + 1) + "th " + "seller");
                main.print("press 2 to accept the " + (u + 1) + "th " + "seller");
                int premission = main.scanInt();
                if (premission == 1)
                    requestlist.getWaitingsellers().remove(u);
                else
                {
                    sellerlist.signupseller(requestlist.getWaitingsellers().get(u));
                    requestlist.getWaitingsellers().remove(u);
                }
                if ( u < requestlist.waitingsellers.size())
                    continue;

            }
            managerpanel();
        }
        if (z == 3) {
            main.print("which one u wanna remove?");
            main.print(" 1.customer ");
            main.print(" 2.seller ");
            int remov = main.scanInt();

            if (remov == 2) {
                for (int ui = 0; ui < sellerlist.getSellers().size(); ui++) {
                    main.print("information of " + ui + 1 + " th seller");
                    main.print(sellerlist.getSellers().get(ui).getName());
                    main.print(sellerlist.getSellers().get(ui).getLastname());
                    main.print(sellerlist.getSellers().get(ui).getEmail());
                    main.print(sellerlist.getSellers().get(ui).getPassword());
                    main.print(sellerlist.getSellers().get(ui).getUsername());
                    main.print(sellerlist.getSellers().get(ui).getPhonenumber());
                    main.print(sellerlist.getSellers().get(ui).getFactoryname());
                    main.print("");
                    main.print("");
                }
                main.print("inter the number of seller u gonna remove");
                int g = main.scanInt();
                sellerlist.getSellers().remove(g);
                managerpanel();
            }
            if (remov == 1) {
                for (int uii = 0; uii < customerlist.getCustomers().size(); uii++) {
                    main.print("information of " + uii + 1 + " th customer");
                    main.print(customerlist.getCustomers().get(uii).getName());
                    main.print(customerlist.getCustomers().get(uii).getLastname());
                    main.print(customerlist.getCustomers().get(uii).getEmail());
                    main.print(customerlist.getCustomers().get(uii).getPassword());
                    main.print(customerlist.getCustomers().get(uii).getUsername());
                    main.print(customerlist.getCustomers().get(uii).getPhonenumber());
                    main.print("");
                    main.print("");
                }
                main.print("inter the number of customer u gonna remove");
                int gg = main.scanInt();
                customerlist.getCustomers().remove(gg);
                managerpanel();
            }
            managerpanel();
        }
        if (z == 4) {
            main.print("which group u wanna see the information?");
            main.print("1.sellers");
            main.print("2.customers");
            int oo = main.scanInt();
            switch (oo) {
                case 1:
                    for (int ui = 0; ui < sellerlist.getSellers().size(); ui++) {
                        main.print("information of " + ui + 1 + " th seller");
                        main.print(sellerlist.getSellers().get(ui).getName());
                        main.print(sellerlist.getSellers().get(ui).getLastname());
                        main.print(sellerlist.getSellers().get(ui).getEmail());
                        main.print(sellerlist.getSellers().get(ui).getPassword());
                        main.print(sellerlist.getSellers().get(ui).getUsername());
                        main.print(sellerlist.getSellers().get(ui).getPhonenumber());
                        main.print(sellerlist.getSellers().get(ui).getFactoryname());
                        main.print("");
                        main.print("");
                    }
                    managerpanel();
                    break;
                case 2:
                    for (int uii = 0; uii < customerlist.getCustomers().size(); uii++) {
                        main.print("information of " + uii + 1 + " th customer");
                        main.print(customerlist.getCustomers().get(uii).getName());
                        main.print(customerlist.getCustomers().get(uii).getLastname());
                        main.print(customerlist.getCustomers().get(uii).getEmail());
                        main.print(customerlist.getCustomers().get(uii).getPassword());
                        main.print(customerlist.getCustomers().get(uii).getUsername());
                        main.print(customerlist.getCustomers().get(uii).getPhonenumber());
                        main.print("");
                        main.print("");
                    }
                    managerpanel();
                    break;
            }
            managerpanel();
        }
        if (z == 5) {
            main.print("u logged out");
            rabetkarbari.safheasli();
        }
        if (z == 6) {
            {
                for (int u = 0; u < addkalarequest.getAddrequest().size(); u++) {
                    main.print("information of " + u + 1 + " th kala");
                    main.print(addkalarequest.getAddrequest().get(u).getName());
                    main.print(addkalarequest.getAddrequest().get(u).getBrand());
                    main.print(addkalarequest.getAddrequest().get(u).getPrice());
                    main.print(addkalarequest.getAddrequest().get(u).getSeller());
                    main.print(addkalarequest.getAddrequest().get(u).getKalaid());

                    main.print("");
                    main.print("");
                }
                for (int b = 0; b < addkalarequest.getAddrequest().size(); b++) {
                    main.print("press 1 to decline the " + (b + 1) + "th " + "kala");
                    main.print("press 2 to accept the " + (b + 1) + "th " + "kala");

                    int premission = main.scanInt();
                    switch (premission) {
                        case 1:
                            addkalarequest.getAddrequest().remove(b);
                            break;
                        case 2:
                            listkala.gotokala(addkalarequest.getAddrequest().get(b));
                            ;
                            addkalarequest.getAddrequest().remove(b);
                            break;
                        default:
                            managerpanel();
                    }
                }
            }
            managerpanel();
        }
    }
}
class RandomString
{

    static String getAlphaNumericString() {

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(13);

        for (int i = 0; i < 14 ; i++) {

            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
}
class randomseason
{
    static String seasons() {

        String seasons ="1234";

        StringBuilder sb = new StringBuilder(1);

        for (int i = 0; i < 1 ; i++) {

            int index
                    = (int) (seasons.length()
                    * Math.random());

            sb.append(seasons
                    .charAt(index));
        }
        return sb.toString();
    }
}
class fasl
{
    static String season = randomseason.seasons();
    static String faasl = season;
}
